# jira-datacenter-fargate

This project is meant to demonstrate provisioning Jira's datacenter on an AWS Fargate cluster. It will use
relatively small instances that can scale up and down according to demand, using AWS's newly released ability
to use an EFS drive in Fargate to store attachments.