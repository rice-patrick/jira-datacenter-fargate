# docker-compose utilities

This folder acts as a method for testing out the docker configurations
locally before deploying them out to AWS. It will use a local
postgres container and volume mount to simulate EFS and ALB.

## First time setup
The below is only necessary if you want the container to run as a non-root user.
While this works fine locally, it does tend to cause a bunch of issues when running on AWS
because of how atlassian has their container set up. It would be much better if they would
set their container up properly, and have the user defined at container time instead of runtime.

### Volume Permissions
Docker-compose doesn't support setting permissions appropriately on named docker containers. In 
order for this to work, you must set the permissions on the named volume manually after you run
the container for the first time. To do so, run docker compose once then execute the following
commands:
```bash
sudo chown :2001 /var/lib/docker/volumes/docker-compose_attachments/_data
sudo chmod 775 /var/lib/docker/volumes/docker-compose_attachments/_data/
sudo chmod g+s /var/lib/docker/volumes/docker-compose_attachments/_data/

sudo chown :2001 /var/lib/docker/volumes/docker-compose_jira-logs/_data
sudo chmod 775 /var/lib/docker/volumes/docker-compose_jira-logs/_data/
sudo chmod g+s /var/lib/docker/volumes/docker-compose_jira-logs/_data/
```
This is a one-time-only required activity, unless you destroy the volume.

### JIRA Configuration
The first time you setup JIRA, it requires a wizard walkthrough to create an administrator
password, along with several other configurations such as timezone. However, there is no
host port mapped in the `docker-compose.yml` file because that prevents the use of the 
`--scale` command when testing your setup for whether it works for scaling. That means in
order to access the jira instance the first time, you have to find the ephemeral port that
docker-compose mapped JIRA to. In order to do that, use the `docker-compose ps` command.

Note that JIRA may take upwards of 5 minutes to fully start.
