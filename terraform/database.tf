module "jira-database" {
  #source = "/home/vagrant/terraform-modules/aws/databases/aurora-serverless-postgres"
  source = "git::https://gitlab.com/rice-patrick/terraform-modules.git//aws/databases/aurora-serverless-postgres"

  cluster_name = "jira"
  database_subnet_ids = data.aws_subnet_ids.db_private_subnets.ids
}


data "aws_subnet_ids" "db_private_subnets" {
  filter {
    name = "tag:${var.subnet_tag_name}"
    values = var.db_subnet_tag_values
  }

  vpc_id = data.aws_vpc.my_vpc.id
}