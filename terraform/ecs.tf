data "aws_vpc" "my_vpc" {
  filter {
    name = "tag:Use"
    values = [var.usage]
  }
}

data "aws_subnet_ids" "ecs_private_subnets" {
  filter {
    name = "tag:${var.subnet_tag_name}"
    values = var.ecs_subnet_tag_values
  }

  vpc_id = data.aws_vpc.my_vpc.id
}

data "aws_subnet_ids" "public_subnets" {
  filter {
    name = "tag:${var.subnet_tag_name}"
    values = var.public_subnet_tag_values
  }

  vpc_id = data.aws_vpc.my_vpc.id
}

#############################
# LOCALS FOR DEFAULTS
#############################

locals {
  connection_string = "${var.jdbc_string_prefix}${module.jira-database.database_dns}:${module.jira-database.database_port}/${module.jira-database.database_db_name}"
}

#############################
# RESOURCES / MODULES
#############################

module "jira" {
  #source = "/home/vagrant/terraform-modules/aws/compute/ecs-fargate-with-efs/"
  source = "git::https://gitlab.com/rice-patrick/terraform-modules.git//aws/compute/ecs-fargate-with-efs"
  cluster-name = "jira"

  container-image = "atlassian/jira-software"
  container-tag = "latest"

  container-memory = var.container_memory
  container-cpu    = var.container_cpu

  vpc_id = data.aws_vpc.my_vpc.id
  ecs_subnets = data.aws_subnet_ids.db_private_subnets.ids
  lb-subnets = data.aws_subnet_ids.public_subnets.ids

  lb-health-check-url  = "/status"
  lb-listener-protocol = "HTTP"
  lb-listener-port     = 80
  lb-sticky-session    = true
  container-startup-time = var.container_grace_time

  container-environment = [
    {name = "ATL_DB_DRIVER", value = var.database_driver},
    {name = "ATL_DB_TYPE", value = var.database_type},
    # Secret comes from secret manager, see container-secrets
    {name = "ATL_JDBC_USER", value = module.jira-database.database_username},
    {name = "ATL_JDBC_URL", value = local.connection_string},
    {name = "ATL_PROXY_NAME", value = module.jira.load-balancer-dns},
    {name = "ATL_PROXY_PORT", value = module.jira.load-balancer-port},
    {name = "ATL_TOMCAT_SCHEME", value = "http"},

    {name = "CLUSTERED", value = "true"}, # this makes us run in data center. No reason to use docker if it's false.
    {name = "JIRA_SHARED_HOME", value = "/data/atlassian"}, # Anything in /data resides on EFS

    # This is really annoying, but until Atlassian fixes their docker image you have to manually update permissions
    # on the shared directory if you don't run as root.
    {name = "RUN_UID", value = "0"},
    {name = "RUN_GID", value = "0"},
    {name = "RUN_USER", value = "root"},
    {name = "RUN_GROUP", value = "root"}
  ]

  container-secrets = [
    {name = "ATL_JDBC_PASSWORD", valueFrom = module.jira-database.database_secret_manager_arn},
  ]
}

# Expose the database to the ECS containers
resource "aws_security_group_rule" "db_to_ecs" {
  from_port = module.jira-database.database_port
  to_port   = module.jira-database.database_port
  protocol  = "TCP"

  source_security_group_id = module.jira.ecs-cluster-security-group-id
  security_group_id = module.jira-database.rds_security_group_id

  type = "ingress"
}

resource "aws_appautoscaling_policy" "scale_jira" {
  name = "target-scale-jira-${var.usage}-request-count"

  policy_type        = "TargetTrackingScaling"
  resource_id        = module.jira.ecs-autoscaling-resource-name
  scalable_dimension = module.jira.ecs-autoscaling-dimension
  service_namespace  = module.jira.ecs-autoscaling-namespace

  target_tracking_scaling_policy_configuration {
    target_value = var.requests_per_container

    scale_in_cooldown  = var.scaling_cooldown
    scale_out_cooldown = var.scaling_cooldown

    predefined_metric_specification {
      predefined_metric_type = "ALBRequestCountPerTarget"
      resource_label = "${data.aws_lb.fargate-lb.arn_suffix}/${data.aws_lb_target_group.fargate-tg.arn_suffix}"
    }
  }
}

data "aws_lb" "fargate-lb" {
  arn = module.jira.load-balancer-arn
}

data "aws_lb_target_group" "fargate-tg" {
  arn = module.jira.load-balancer-tg-arn
}