variable "usage" {
  default = "dev"
  description = "The environment level to use. Defaults to dev."
}

variable "subnet_tag_name" {
  description = "The name of the tag to use when finding subnets that are open"
  default     = "GenericId"
}

variable "ecs_subnet_tag_values" {
  description = "The values to find ecs private subnets"
  type        = list
  default     = ["private-0-az-0", "private-0-az-1", "private-1-az-0", "private-1-az-1"]
}

variable "db_subnet_tag_values" {
  description = "The values to find db private subnets"
  type        = list
  default     = ["private-2-az-0", "private-2-az-1"]
}

variable "public_subnet_tag_values" {
  description = "The values to find public subnets"
  type        = list
  default     = ["public*"]
}


variable "postgres_auto_pause" {
  default = "false"
  description = "whether or not the database should spin down when there is no activity"
}

variable "postgres_max_capacity" {
  default = "16"
  description = "Maximum number of ACU to use for the database"
}

variable "postgres_min_capacity" {
  default = "2"
  description = "Minimum number of ACU to use for the database"
}

variable "vpc_id" {
  default = "which VPC to use"
}

###########################
# Jira environment variables
###########################

variable "database_driver" {
  default = "org.postgresql.Driver"
  description = "The Database driver to use within JIRA"
}

variable "database_type" {
  default = "postgres72"
  description = "Type of database to use. Not sure why they don't infer drivers from this, but ask atlassian not me."
}

variable "jdbc_string_prefix" {
  default = "jdbc:postgresql://"
  description = "The prefix for the JDBC url. Will be concatenated with the url and ports."
}

##############################
# Container and LB configuration
##############################

variable "container_memory" {
  description = "How much memory to use when running JIRA"
  default     = 8192
}

variable "container_cpu" {
  description = "How much memory to use when running JIRA"
  default     = 4096
}

variable "container_grace_time" {
  description = "how long to wait with a failing health check to register healthy for the first time before terminating the contianer"
  # The reason this is set to 1200 (20m) is because there is manual intervention required to register
  # the license the first time JIRA is set up. Once that manual intervention is completed, this can
  # safely be set to 400 (a little over 5m) if desired.
  default = 1200
}

variable "requests_per_container" {
  description = "How many requests each container can handle. Used to scale containers"
  default = 5000
}

variable "scaling_cooldown" {
  description = "How long to wait after a scaling activity before the cluster will scale again"
  # Jira takes ~3 min to start up, so let it go for long enough that new requests are routed to the new containers
  default = 300
}

variable "load_balancer_protocol" {
  description = "Which http/https scheme to use for the load balancer"
  default     = "http" # Mostly because you have to provision a certificate for https
}